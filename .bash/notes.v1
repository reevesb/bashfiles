#!/bin/bash
#===============================================================================
#
#          FILE:  .notes
# 
#         USAGE:  ./.notes 
# 
#   DESCRIPTION:  vim ; ag based note taking and search 
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  12/11/2018 03:29:50 PM MDT
#      REVISION:  ---
#===============================================================================


#===============================================================================
# function opps
#===============================================================================
opps() {
  shopt -s extglob
  string="@(${files[0]}"
  for((i=1;i<${#files[@]};i++))
  do
    string+="|${files[$i]}"
  done
  string+=")"

# Show the menu. This will list all files and the string "quit"
  select file in "${files[@]}" "quit"
  do
    case $file in
    # If the choice is one of the files (if it matches $string)
    $string)
      gvim "+normal G$" ${file} 
      break;
      ;;

    "quit")
       # Exit
       break;;
    *)
       file=""
       echo "Please choose a number from 1 to $((${#files[@]}+1))";;
    esac
done
}

#===============================================================================
# timebox   ( text )
#===============================================================================
function timebox(){
  printf "┏━━━━━━━━━━━━┓\n"
  printf "┃ "
  printf '\xF0\x9F\x95\x94  '
  printf `date +%R`
  printf "          ┃\n"
  printf "┗━━━━━━━━━━━━┛\n"
}

#===============================================================================
# timebox.md  
#===============================================================================
function timebox.md(){
  printf "\n"
  printf "---"
  printf "\`\`\`"
  printf `date +%R`
  printf "\`\`\`  \n\n"
  printf "---"
}

#===============================================================================
# note_header  ( text )
#===============================================================================
function note_header(){
 if [ -e  "${CURRENT}/${DAY}${EXT}" ]; then
    echo "no header"
 else
   echo "___" >> ${CURRENT}/${DAY}${EXT}
   echo "# ${CURRENT}/${DAY}${EXT}" >> ${CURRENT}/${DAY}${EXT}
   echo "___" >> ${CURRENT}/${DAY}${EXT}
 fi
}

#===============================================================================
# note_header.md  ( markdown )
#===============================================================================
function note_header.md(){
 if [ -e  "${CURRENT}/${DAY}${EXT}" ]; then
    echo "no header"
 else
   #echo "# ${CURRENT}/${DAY}" >> ${CURRENT}/${DAY}${EXT}  
   echo "# ${TODAY}" >> ${CURRENT}/${DAY}${EXT}  
 fi
}


function nw() {
  if [ -z $1 ]; then
    echo "Usage nw {type = [v|vs]} [ option ]"
  fi
}

#===============================================================================
# notes
#===============================================================================
function notes() {
  # If there is a Dropbox setup on the box, use the notes dir there
  if [ -d "${HOME}/Dropbox" ]; then
    NoteHome=${HOME}/Dropbox
  else
    NoteHome=${HOME}/Documents/Personal
  fi

  CURRENT="${NoteHome}/Notes/`date +%Y`/`date +%B`"
  DAY="`date +%d`"
  TODAY="`date +%Y`/`date +%B`/`date +%d`"
  EXT=".md"

  mkdir -p ${CURRENT}

  start_dir=`pwd`
  
    case $1 in
      add) echo >> ${CURRENT}/${DAY}${EXT};
           echo >> ${CURRENT}/${DAY}${EXT};
           printf "\xF0\x9F\x95\x90 `date +%R`\n" >> ${CURRENT}/${DAY}${EXT};
           printf "\n" >> ${CURRENT}/${DAY}${EXT};
           printf "\xE2\x80\xA2  " >> ${CURRENT}/${DAY}${EXT};
           cat ${start_dir}/$2 >> ${CURRENT}/${DAY}${EXT} ;;
      osearch) grep -Ri $2 ${NoteHome}/Notes ;;
      list) files=(`grep -Ri $2 ${NoteHome}/Notes | awk -F':' '{print $1}' | sort -u`)
            echo ${files}
            ;;
      s2)  ag --filename $2 ${NoteHome}/Notes 
           files=(`ag -l $2 ${NoteHome}/Notes`)
           ;;
      search)  file=`ag --filename $2 ${NoteHome}/Notes | peco | awk -F':' '{print $1}'`
            gvim "+normal G$" ${file} 
            ;;
      tags)  file=`ag --filename '#([^\s]+)' ${NoteHome}/Notes | peco | awk -F':' '{print $1}'`
            gvim "+normal G$" ${file} 
            ;;
      *)   note_header.md
           timebox.md >> ${CURRENT}/${DAY}${EXT};
           gvim "+normal G$" -c 'startinsert' ${CURRENT}/${DAY}${EXT} ;;
      esac

  }




#===============================================================================
# vsnotes
#===============================================================================
function vnotes() {
  # If there is a Dropbox setup on the box, use the notes dir there
  if [ -d "${HOME}/Dropbox" ]; then
    NoteHome=${HOME}/Dropbox
  else
    NoteHome=${HOME}/Documents/Personal
  fi

  CURRENT="${NoteHome}/Notes/`date +%Y`/`date +%B`"
  DAY="`date +%d`"
  EXT=".md"

  mkdir -p ${CURRENT}

  start_dir=`pwd`
  
    case $1 in
      add) echo >> ${CURRENT}/${DAY}${EXT};
           echo >> ${CURRENT}/${DAY}${EXT};
           printf "\xF0\x9F\x95\x90 `date +%R`\n" >> ${CURRENT}/${DAY}${EXT};
           printf "\n" >> ${CURRENT}/${DAY}${EXT};
           printf "\xE2\x80\xA2  " >> ${CURRENT}/${DAY}${EXT};
           cat ${start_dir}/$2 >> ${CURRENT}/${DAY}${EXT} ;;
      osearch) grep -Ri $2 ${NoteHome}/Notes ;;
      list) files=(`grep -Ri $2 ${NoteHome}/Notes | awk -F':' '{print $1}' | sort -u`)
            opps;;
      s2)  ag --filename $2 ${NoteHome}/Notes 
           files=(`ag -l $2 ${NoteHome}/Notes`)
           opps;;
      search)  file=`ag --filename $2 ${NoteHome}/Notes | peco | awk -F':' '{print $1}'`
            typora ${file} 
            ;;
      *)   note_header${ext} 
           echo >> ${CURRENT}/${DAY}${EXT};
           timebox.md >> ${CURRENT}/${DAY}${EXT};
           #printf "\n" >> ${CURRENT}/${DAY}${EXT};
           #printf "\xE2\x80\xA2  " >> ${CURRENT}/${DAY}${EXT};
           typora ${CURRENT}/${DAY}${EXT} ;;
      esac

  }

# TODO - parse command line arguments to refactor duplication
#function notes() {
#  case $1 in
#    vim) shift 
#          vim_notes $* ;;
#    vs)  shift ; vs_notes $* ;;
#    *)   shift ; vim_notes $* ;;
#  esac 
#}


#while (( "$#" )); do
#
#if [[ $(ls "$1") == "" ]]; then 
#	echo "Empty directory, nothing to be done."
#  else 
#	find "$1" -type f -a -atime +365 -exec rm -i {} \;
#fi
#shift

function usage() {
  echo votes [operation] [editor_type]
}

function votes() {
    echo $#
    if [[ $# -gt 2 ]]; then
      echo "too few args";
    fi
}
