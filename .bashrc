#!/bin/bash
#===============================================================================
#
#          FILE:  .bashrc
# 
#         USAGE:  ./.bashrc 
# 
#   DESCRIPTION:  Bash Setup. Breaks the various bash env setups to seperate configs 
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  individual bash setup files located in ~/.bash/
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Brad Reeves (brad.reeves@gmail.com), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  07/19/2007 10:03:41 AM MDT
#      REVISION:  ---
#===============================================================================

# Quick Order Reference : system bashrc : path ; environment ; java classpath : work : alias : projects : shell functions : complete 


# Source global definitions
if [ -f /etc/bashrc ]; then
	source /etc/bashrc
fi

# Source global definitions
#if [ -f .bash_profile ]; then
#	. .bash_profile
#fi

# Source the path file.
if [ -f ~/.bash/path ]; then
	source ~/.bash/path
fi

# Source the basic bash environment.
if [ -f ~/.bash/environment ]; then
	source ~/.bash/environment
fi

# Shell functions.
if [ -f ~/.bash/shellfunc ]; then
	source ~/.bash/shellfunc
fi


# Source the JAVA classpath
if [ -f ~/.bash/classpath ]; then
	source ~/.bash/classpath
fi

# Source aliases
if [ -f ~/.bash/alias ]; then
	source ~/.bash/alias
fi

## Project
if [ -f ~/.bash/projects ]; then
	source ~/.bash/projects
fi

# VIM Notes
if [ -f ~/.bash/notes ]; then
	source ~/.bash/notes
fi

## Work specific stuff {project homes, project aliases etc...}
if [ -f ~/.bash/work ]; then
	source ~/.bash/work
fi

## kubernetes specific stuff 
if [ -f ~/.bash/kuber ]; then
	source ~/.bash/kuber
fi

## aws 
if [ -f ~/.bash/aws ]; then
	source ~/.bash/aws
fi

# Shell completions
if [ -f ~/.bash/complete ]; then
	source ~/.bash/complete
fi

# cdargs
if [ -f ~/.bash/cdargs-bash.sh ]; then
	source ~/.bash/cdargs-bash.sh
fi

# bash style
#if [ -f ~/.bash/.bash_style_rc ]; then
#      source ~/.bash/.bash_style_rc
#fi


#if [ -f /usr/share/powerline/bindings/bash/powerline.sh ]; then
#	source /usr/share/powerline/bindings/bash/powerline.sh
##fi

# bash style
if [ -f ~/.bash/system ]; then
	source ~/.bash/system
fi

# Ruby ENV rbenv
# if [ -f ~/.bash/rbenv ]; then
#	source ~/.bash/rbenv
# fi

# node 
if [ -f ~/.bash/node ]; then
	source ~/.bash/node
fi

# GO ENV go
if [ -f ~/.bash/golang ]; then
	source ~/.bash/golang
fi

# Source the pyenv file.
if [ -f ~/.bash/pyenv ]; then
	source ~/.bash/pyenv
fi

#Powerline
if [ -f `which powerline-daemon` ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
  . /Library/Python/3.8/site-packages/powerline/bindings/bash/powerline.sh
fi
#. /usr/share/powerline/bindings/bash/powerline.sh
#
##THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/bradleyr/.sdkman"
[[ -s "/home/bradleyr/.sdkman/bin/sdkman-init.sh" ]] && source "/home/bradleyr/.sdkman/bin/sdkman-init.sh"
fortune
